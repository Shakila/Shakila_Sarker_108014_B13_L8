<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Student;
use \App\Bitm\SEIP108014\Utility\Utility;

$obj = new Student();
$thepb = $obj->edit($_GET['id'], $_GET['gender']);
//var_dump($thepb);
//die();
?>

<html>

       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <title>Update Student Info</title>
              <link rel="stylesheet" type="text/css" href="../../../css/birthdayStyle.css">
              <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
       </head>
       <body>
              <div class="container">
                     <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                   <h3>Student Information</h3>
                            </div>
                            <div class="col-md-12 col-xs-4 col-sm-8">
                                   <form action="update.php" method="post" class="form-horizontal" role="form">
                                          <input type="hidden" name="ID" value="<?php echo $thepb->ID; ?>" /> 
                                          <div class="form-group">
                                                 <label class="control-label col-sm-2" for="StudentName">Student Name:</label>
                                                 <div class="col-sm-6">          
                                                        <input type="text" class="form-control" name="StudentName" value="<?php echo $thepb->StudentName; ?>" id="StudentName" placeholder="Enter Student's Name">
                                                 </div>
                                          </div>

                                          <div class="form-group">
                                                 <label class="control-label col-sm-2" for="StudentID">Student ID:</label>
                                                 <div class="col-sm-6">          
                                                        <input type="text" class="form-control" name="StudentID" value="<?php echo $thepb->StudentID; ?>" id="StudentID" placeholder="Enter Student's ID">
                                                 </div>
                                          </div>

                                          <div class="form-group">
                                                 <label class="control-label col-sm-2" for="Gender">Gender:</label>
                                                 <div class="radio">
                                                        <label  class="control-label col-sm-2"><input type="radio" name="Gender" value="Male" <?php if ($thepb->Gender == 'Male') echo "checked" ?>>Male</label>

                                                        <label  class="control-label col-sm-2"><input type="radio" name="Gender" value="Female" <?php if ($thepb->Gender == 'Female') echo "checked" ?>>Female</label>
                                                 </div>
                                          </div>
                                          <br></br>
                                          <div class="form-group">        
                                                 <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-sucsess">Submit</button>
                                                        <button type="submit" class="btn btn-sucsess"><a href="index.php">Home</a></button>
                                                 </div>
                                          </div>
                                   </form>
                            </div>
                     </div>
              </div>

              <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <!-- Include all compiled plugins (below), or include individual files as needed -->
              <script src="../../../resource/js/bootstrap.min.js"></script>
       </body>
</html>
