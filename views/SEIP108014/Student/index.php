<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Student;
use \App\Bitm\SEIP108014\Utility\Utility;

$obj = new Student();
$var = $obj->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information List</title>
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
   
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h2>Student List</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Name</th>
                                <th>ID</th>
                                <th>Gender</th>
                                <th>Action</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sl = 0;
                            foreach ($var as $pbook) {
                                $sl++;
                                ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $pbook['StudentName']; ?></td>
                                    <td><?php echo $pbook['StudentID']; ?></td>
                                    <td><?php echo $pbook['Gender']; ?></td>
                                    <td><button type="submit" class="btn btn-warning"><a href="edit.php?id=<?php echo $pbook['ID']; ?>&gender=<?php echo $pbook['Gender']; ?>">Update</a></button> </td>
                                    <td>    
                                        <form action="delete.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $pbook['ID']; ?>">
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="create.php"><b>Entry Student Data</b></a>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
